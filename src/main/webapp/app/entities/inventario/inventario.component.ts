import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { filter, map } from 'rxjs/operators';
import { JhiEventManager } from 'ng-jhipster';

import { IInventario } from 'app/shared/model/inventario.model';
import { AccountService } from 'app/core/auth/account.service';
import { InventarioService } from './inventario.service';

@Component({
  selector: 'jhi-inventario',
  templateUrl: './inventario.component.html'
})
export class InventarioComponent implements OnInit, OnDestroy {
  inventarios: IInventario[];
  currentAccount: any;
  eventSubscriber: Subscription;
  currentSearch: string;

  constructor(
    protected inventarioService: InventarioService,
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll() {
    if (this.currentSearch) {
      this.inventarioService
        .search({
          query: this.currentSearch
        })
        .pipe(
          filter((res: HttpResponse<IInventario[]>) => res.ok),
          map((res: HttpResponse<IInventario[]>) => res.body)
        )
        .subscribe((res: IInventario[]) => (this.inventarios = res));
      return;
    }
    this.inventarioService
      .query()
      .pipe(
        filter((res: HttpResponse<IInventario[]>) => res.ok),
        map((res: HttpResponse<IInventario[]>) => res.body)
      )
      .subscribe((res: IInventario[]) => {
        this.inventarios = res;
        this.currentSearch = '';
      });
  }

  search(query) {
    if (!query) {
      return this.clear();
    }
    this.currentSearch = query;
    this.loadAll();
  }

  clear() {
    this.currentSearch = '';
    this.loadAll();
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().subscribe(account => {
      this.currentAccount = account;
    });
    this.registerChangeInInventarios();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IInventario) {
    return item.id;
  }

  registerChangeInInventarios() {
    this.eventSubscriber = this.eventManager.subscribe('inventarioListModification', response => this.loadAll());
  }
}
