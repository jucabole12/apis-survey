import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ServiciosSharedModule } from 'app/shared/shared.module';
import { InventarioComponent } from './inventario.component';
import { InventarioDetailComponent } from './inventario-detail.component';
import { InventarioUpdateComponent } from './inventario-update.component';
import { InventarioDeletePopupComponent, InventarioDeleteDialogComponent } from './inventario-delete-dialog.component';
import { inventarioRoute, inventarioPopupRoute } from './inventario.route';

const ENTITY_STATES = [...inventarioRoute, ...inventarioPopupRoute];

@NgModule({
  imports: [ServiciosSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    InventarioComponent,
    InventarioDetailComponent,
    InventarioUpdateComponent,
    InventarioDeleteDialogComponent,
    InventarioDeletePopupComponent
  ],
  entryComponents: [InventarioDeleteDialogComponent]
})
export class ServiciosInventarioModule {}
