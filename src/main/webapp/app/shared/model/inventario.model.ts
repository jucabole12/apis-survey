export interface IInventario {
  id?: number;
  nombre?: string;
  precio?: number;
  unidades?: number;
}

export class Inventario implements IInventario {
  constructor(public id?: number, public nombre?: string, public precio?: number, public unidades?: number) {}
}
