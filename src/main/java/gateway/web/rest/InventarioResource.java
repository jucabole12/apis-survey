package gateway.web.rest;

import gateway.domain.Inventario;
import gateway.repository.InventarioRepository;
import gateway.repository.search.InventarioSearchRepository;
import gateway.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link gateway.domain.Inventario}.
 */
@RestController
@RequestMapping("/api")
public class InventarioResource {

    private final Logger log = LoggerFactory.getLogger(InventarioResource.class);

    private static final String ENTITY_NAME = "inventario";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InventarioRepository inventarioRepository;

    private final InventarioSearchRepository inventarioSearchRepository;

    public InventarioResource(InventarioRepository inventarioRepository, InventarioSearchRepository inventarioSearchRepository) {
        this.inventarioRepository = inventarioRepository;
        this.inventarioSearchRepository = inventarioSearchRepository;
    }

    /**
     * {@code POST  /inventarios} : Create a new inventario.
     *
     * @param inventario the inventario to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new inventario, or with status {@code 400 (Bad Request)} if the inventario has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/inventarios")
    public ResponseEntity<Inventario> createInventario(@Valid @RequestBody Inventario inventario) throws URISyntaxException {
        log.debug("REST request to save Inventario : {}", inventario);
        if (inventario.getId() != null) {
            throw new BadRequestAlertException("A new inventario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Inventario result = inventarioRepository.save(inventario);
        inventarioSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/inventarios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /inventarios} : Updates an existing inventario.
     *
     * @param inventario the inventario to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated inventario,
     * or with status {@code 400 (Bad Request)} if the inventario is not valid,
     * or with status {@code 500 (Internal Server Error)} if the inventario couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/inventarios")
    public ResponseEntity<Inventario> updateInventario(@Valid @RequestBody Inventario inventario) throws URISyntaxException {
        log.debug("REST request to update Inventario : {}", inventario);
        if (inventario.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Inventario result = inventarioRepository.save(inventario);
        inventarioSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, inventario.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /inventarios} : get all the inventarios.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of inventarios in body.
     */
    @GetMapping("/inventarios")
    public List<Inventario> getAllInventarios() {
        log.debug("REST request to get all Inventarios");
        return inventarioRepository.findAll();
    }

    /**
     * {@code GET  /inventarios/:id} : get the "id" inventario.
     *
     * @param id the id of the inventario to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the inventario, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/inventarios/{id}")
    public ResponseEntity<Inventario> getInventario(@PathVariable Long id) {
        log.debug("REST request to get Inventario : {}", id);
        Optional<Inventario> inventario = inventarioRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(inventario);
    }

    /**
     * {@code DELETE  /inventarios/:id} : delete the "id" inventario.
     *
     * @param id the id of the inventario to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/inventarios/{id}")
    public ResponseEntity<Void> deleteInventario(@PathVariable Long id) {
        log.debug("REST request to delete Inventario : {}", id);
        inventarioRepository.deleteById(id);
        inventarioSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/inventarios?query=:query} : search for the inventario corresponding
     * to the query.
     *
     * @param query the query of the inventario search.
     * @return the result of the search.
     */
    @GetMapping("/_search/inventarios")
    public List<Inventario> searchInventarios(@RequestParam String query) {
        log.debug("REST request to search Inventarios for query {}", query);
        return StreamSupport
            .stream(inventarioSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
    /*
    * @return api que permite la insersion de multiples objetos
    * creado por cristian y sebastian
    * */
    @PostMapping("/inventarios/objetos")
    public ResponseEntity<List<Inventario>> saveObjetos(@RequestBody List<Inventario> lista){
        List<Inventario> newList = new ArrayList<Inventario>();
        for (Inventario item: lista){
            Inventario saved = inventarioRepository.save(item);
            if (saved!=null){
                newList.add(saved);
            }
        }
        return ResponseEntity.ok().body(newList);
    }
}
