package gateway.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link InventarioSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class InventarioSearchRepositoryMockConfiguration {

    @MockBean
    private InventarioSearchRepository mockInventarioSearchRepository;

}
