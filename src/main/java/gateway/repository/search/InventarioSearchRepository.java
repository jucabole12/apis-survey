package gateway.repository.search;
import gateway.domain.Inventario;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Inventario} entity.
 */
public interface InventarioSearchRepository extends ElasticsearchRepository<Inventario, Long> {
}
